<?php

namespace app\modules\v1\modules\user_wallet\models;

use yiier\helpers\Setup;

/**
 * Class UserWalletService
 * @package app\modules\v1\modules\user_wallet\models
 */
class UserWalletService
{

    /**
     * @param UserWalletTransaction $transaction
     * @return bool
     */
    public function updateValueByTransaction(UserWalletTransaction $transaction): bool
    {
        // transfer current sum to user currency
        $userWallet = $transaction->wallet;
        if (!$userWallet) throw new \yii\db\Exception("user_wallet_not_found");

        $walletCurrency = $userWallet->currency;
        if (!$walletCurrency) throw new \yii\db\Exception("wallet_currency_not_found");

        $fromCurrency = $transaction->currency;
        if (!$fromCurrency) throw new \yii\db\Exception("transaction_currency_not_found");

        $sum = CurrencyService::transferToCurrency($fromCurrency, $walletCurrency, $transaction->sum);

        // debt or credit user wallet value
        if ($transaction->type == UserWalletTransaction::TYPE_DEBIT){
            $userWallet->value += $sum;
        } elseif ($transaction->type == UserWalletTransaction::TYPE_CREDIT){
            $userWallet->value -= $sum;
        }

        // in case < 0 then zeroing wallet value (cause: we cannot go in minus)
        if ($userWallet->value < 0 ) $userWallet->value = 0;
        $isUpdated = $userWallet->update(true, ['value']);
        if (!$isUpdated && !empty($userWallet->errors)){
            throw new \yii\db\Exception(Setup::errorMessage($userWallet->firstErrors));
        }
        return $isUpdated;
    }
}
