<?php

namespace app\modules\v1\modules\user_wallet\models;

use app\core\models\User;
use Yii;

/**
 * This is the model class for table "user_wallet_transaction".
 *
 * @property int $id
 * @property int $wallet_id
 * @property int $currency_id
 * @property string $type
 * @property int|null $sum
 * @property string $reason
 * @property string $created_at
 * @property string $updated_at
 *
 * @property-read User $user
 * @property-read UserWallet $wallet
 * @property-read Currency $currency
 */
class UserWalletTransaction extends \yii\db\ActiveRecord
{
    const TYPE_DEBIT = 'debit';
    const TYPE_CREDIT = 'credit';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_wallet_transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['wallet_id', 'currency_id', 'type', 'reason'], 'required'],
            [['wallet_id', 'currency_id', 'sum'], 'integer'],
            [['type', 'reason'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'wallet_id' => Yii::t('app', 'Wallet ID'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'type' => Yii::t('app', 'Type'),
            'sum' => Yii::t('app', 'Sum'),
            'reason' => Yii::t('app', 'Reason'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        // only in case of insert
        if ($this->isNewRecord){
            try {
                // start transaction
                Yii::$app->getDb()->beginTransaction();

                // save current wallet transaction
                $isSavedWalletTransaction = parent::save($runValidation, $attributeNames);

                // update user wallet value
                $userWalletService = new UserWalletService();
                $isUpdatedUserWallet = $userWalletService->updateValueByTransaction($this);

                // commit or rollback
                if ($isSavedWalletTransaction && $isUpdatedUserWallet){
                    Yii::$app->getDb()->getTransaction()->commit();
                    return $isSavedWalletTransaction;
                } else {
                    Yii::$app->getDb()->getTransaction()->rollBack();
                    return false;
                }
            } catch (\Throwable $e){
                Yii::$app->getDb()->getTransaction()->rollBack();
                Yii::error(
                    ['request_id' => Yii::$app->requestId->id, $this->attributes, $this->errors, (string)$e],
                    __FUNCTION__
                );
                throw new \yii\db\Exception("cannot_process_wallet_transaction");
            }
        } else {
            return parent::save($runValidation, $attributeNames);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWallet()
    {
        return $this->hasOne(UserWallet::class, ['id' => 'wallet_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['user_id' => 'id'])->via('wallet');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['id' => 'currency_id']);
    }
}
