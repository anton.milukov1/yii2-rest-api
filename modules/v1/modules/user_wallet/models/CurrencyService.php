<?php

namespace app\modules\v1\modules\user_wallet\models;

/**
 * Class CurrencyService
 * @package app\modules\v1\modules\user_wallet\models
 */
class CurrencyService
{
    /**
     * @param Currency $fromCurrency
     * @param Currency $toCurrency
     * @param float $sum
     * @return float
     */
    public static function transferToCurrency(Currency $fromCurrency, Currency $toCurrency, float $sum): float
    {
        if ($fromCurrency->primaryKey == $toCurrency->primaryKey) return $sum;

        // transfer to main currency
        $mainSum = $sum / $fromCurrency->value;

        // transfer to target currency
        $resultSum = $mainSum * $toCurrency->value;
        return (float)$resultSum;
    }
}
