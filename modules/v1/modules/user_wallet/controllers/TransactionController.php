<?php

namespace app\modules\v1\modules\user_wallet\controllers;

use app\modules\v1\controllers\ActiveController;
use app\modules\v1\modules\user_wallet\models\UserWalletTransaction;

class TransactionController extends ActiveController
{
    public $modelClass = UserWalletTransaction::class;

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['update'], $actions['index'], $actions['delete']);
        return $actions;
    }
}
