<?php

namespace app\modules\v1\modules\user_wallet\controllers;

use app\core\models\User;
use app\modules\v1\controllers\ActiveController;
use app\modules\v1\modules\user_wallet\models\UserWallet;

/**
 * Default controller for the `UserWallet` module
 */
class DefaultController extends ActiveController
{
    public $modelClass = UserWallet::class;

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'], $actions['update'], $actions['create'], $actions['delete']);
        return $actions;
    }

    /**
     * wallet data
     * @return string
     */
    public function actionIndex()
    {
        /** @var User $user */
        $user = \Yii::$app->getUser()->getIdentity();
        return $user->wallet;
    }
}
