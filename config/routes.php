<?php
return [
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => 'v1/user-wallet/transaction',
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => 'v1/user-wallet/default',
    ],

    "POST <module>/<alias:login|join|refresh-token>" => '<module>/user/<alias>',
    "GET health-check" => 'site/health-check',
    '<module>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
];
