<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_wallet_transaction}}`.
 */
class m220615_090323_create_user_wallet_transaction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_wallet_transaction}}', [
            'id' => $this->primaryKey(),
            'wallet_id' => $this->integer()->unsigned()->notNull(),
            'currency_id' => $this->integer()->unsigned()->notNull(),
            'type' => "ENUM('debit', 'credit') not null",
            'sum' => $this->float(2)->defaultValue(0),
            'reason' => "ENUM('stock', 'refund') not null",
            'created_at' => \yii\db\Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => \yii\db\Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_wallet_transaction}}');
    }
}
