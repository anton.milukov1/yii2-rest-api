<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_wallet}}`.
 */
class m220615_082239_create_user_wallet_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_wallet}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned()->defaultValue(0)->unique(),
            'currency_id' => $this->integer()->unsigned()->defaultValue(1),
            'value' => $this->float(2)->defaultValue(0),
            'created_at' => \yii\db\Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => \yii\db\Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_wallet}}');
    }
}
