<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 REST API Wallet Module Example</h1>
    <br>
</p>

Clone with git

```sh
git clone https://gitlab.com/anton.milukov1/yii2-rest-api.git
```

Copy .env

```sh
cd yii2-rest-api
cp .env.example .env
```

Update your vendor packages

```sh
docker-compose run --rm php composer update --prefer-dist
```

Run the installation triggers (creating cookie validation code)

```sh
docker-compose run --rm php composer install    
```

Start the container

```sh
docker-compose up -d
```

Run migrations
```sh
docker-compose exec php php yii migrate
```

Run tests
```sh
docker-compose exec php ./vendor/bin/codecept run api WalletCest
```
   
Api docs
```
http://127.0.0.1:8000/doc
```

PhpMyAdmin
```
http://127.0.0.1:8081
* server: mysql
* login: user
* password: user
```

Database access via PhpStorm Database plugin 
```
url: jdbc:mysql://localhost:3306/yii2
user: user
password: user
```

Use
------------

At this time, you have a RESTful API server running at `http://127.0.0.1:8000`. It provides the following endpoints:

* `POST /v1/join`: create a user
* `POST /v1/login`: authenticates a user and generates a JWT
* `POST /v1/refresh-token`: refresh a JWT
* `POST /v1/user-wallet/transaction`: create user wallet transaction 
* `GET /v1/user-wallet`: get current user wallet data

SQL запрос Написать SQL запрос, который вернет сумму, полученную по причине refund за последние 7 дней.
```mysql
# In main currency:
select
    SUM(CASE WHEN c.value = 1 THEN sum WHEN c.value != 1 THEN (sum/c.value) END) AS sum
from user_wallet_transaction as uwt
join currency c on c.id = uwt.currency_id
where uwt.reason = 'refund' and uwt.created_at > DATE_SUB(NOW(), INTERVAL 7 day);
```

Task description
------------
![](task.png)
