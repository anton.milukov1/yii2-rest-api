<?php

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
 * @codingStandardsIgnoreFile
 */
class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;

    /**
     * Define custom actions here
     */

    /**
     * @param ApiTester $I
     * @param array $input
     * @return bool
     * @throws \yii\db\StaleObjectException
     */
    public function createCurrency(ApiTester $I, array $input): bool
    {
        foreach ($input as $currInput){
            $currency = $I->getExistedOrCreate($I, ['name' => $currInput['name']], new \app\modules\v1\modules\user_wallet\models\Currency($currInput));
            $I->assertTrue($currency->primaryKey > 0);
        }
        return true;
    }

    /**
     * @param ApiTester $I
     * @param array $options
     * @return \app\core\models\User
     */
    public function createUser(ApiTester $I, array $options): \app\core\models\User
    {
        $userService = new \app\core\services\UserService();
        $userName = 'codeception_test';
        $userService->deleteUserByUsername($userName);
        $walletCurrencyId = isset($options['currency'])
            ? \app\modules\v1\modules\user_wallet\models\Currency::findOne(['name' => $options['currency']])->primaryKey
            : (\app\modules\v1\modules\user_wallet\models\Currency::find()->one())->primaryKey;

        $user = $userService->createUser(new \app\core\requests\JoinRequest([
            'username' => $userName,
            'email' => 'codeception@test.com',
            'password' => '123',
            'walletCurrencyId' => $walletCurrencyId
        ]));
        $I->assertTrue($user->primaryKey > 0);
        return $user;
    }

    /**
     * @param ApiTester $I
     * @param \app\core\models\User $user
     * @return string
     */
    public function getUserToken(ApiTester $I, \app\core\models\User $user): string
    {
        $userService = new \app\core\services\UserService();
        $token = (string)$userService->getToken($user);
        $I->assertTrue(strlen($token) > 0);
        return $token;
    }

    /**
     * @param ApiTester $I
     * @param array $whereData
     * @param \yii\db\ActiveRecord $model
     * @param false $isForce
     * @return \yii\db\ActiveRecord|null
     * @throws \yii\db\StaleObjectException
     */
    public function getExistedOrCreate(ApiTester $I, array $whereData = [], \yii\db\ActiveRecord $model, $isForce = false): ?\yii\db\ActiveRecord
    {
        $query = $model::find();
        if (!empty($whereData)) {
            $query->where($whereData);
        }
        $existed = $query->one();
        if ($isForce && $existed){
            $existed->delete();
            $existed = null;
        }
        if (!$existed) {
            $saved = $model->save();
            $existed = $saved ? $model : null;
        }
        $class = get_class($model);
        $I->assertSame(true, (isset($existed->primaryKey) && $existed->primaryKey > 0), "check model on exists: {$class}");
        return $existed;
    }
}
