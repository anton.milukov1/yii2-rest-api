<?php

class WalletCest
{
    public function _before(ApiTester $I)
    {
        // @codingStandardsIgnoreEnd
    }

    /**
     * @param ApiTester|\Helper\Api $I
     * @param \Codeception\Example $example
     *
     * @example [{"currency": [{"name": "rub", "value": 100}, {"name": "usd", "value": 1}], "user": {"currency": "rub"}, "input": [{"currency_name": "rub", "sum": "100", "type": "debit", "reason": "stock"}], "expected":{"wallet_value": 100}}]
     * @example [{"currency": [{"name": "rub", "value": 100}, {"name": "usd", "value": 1}], "user": {"currency": "usd"}, "input": [{"currency_name": "rub", "sum": "100", "type": "debit", "reason": "stock"}, {"currency_name": "usd", "sum": "1", "type": "debit", "reason": "stock"}], "expected":{"wallet_value": 2}}]
     * @example [{"currency": [{"name": "rub", "value": 100}, {"name": "usd", "value": 1}], "user": {"currency": "usd"}, "input": [{"currency_name": "rub", "sum": "100", "type": "debit", "reason": "stock"}, {"currency_name": "usd", "sum": "1", "type": "debit", "reason": "stock"}], "expected":{"wallet_value": 2}}]
     */
    public function tryChangeWallet(ApiTester $I, \Codeception\Example $example)
    {
        // init
        $testData = $example[0];
        $input = $testData['input'];
        $expected = $testData['expected'];
        $I->createCurrency($I, $testData['currency']);
        /** @var \app\core\models\User $user */
        $user = $I->createUser($I, $testData['user']);
        $I->assertNotEmpty($user);

        // make update request
        foreach ($input as $payload){
            /** @var \app\modules\v1\modules\user_wallet\models\Currency $currency */
            $currency = \app\modules\v1\modules\user_wallet\models\Currency::findOne(['name' => $payload['currency_name']]);
            $payload = array_merge($payload, [
                'wallet_id' => $user->wallet->primaryKey,
                'currency_id' => $currency->primaryKey
            ]);
            $I->haveHttpHeader('Authorization', "Bearer {$I->getUserToken($I, $user)}");
            $I->sendPost('/v1/user-wallet/transaction', $payload);
            $I->seeResponseIsJson();
            $I->seeResponseCodeIs(\Codeception\Util\HttpCode::CREATED);
        }

        // check user balance through api request
        $I->haveHttpHeader('Authorization', "Bearer {$I->getUserToken($I, $user)}");
        $I->sendGet('/v1/user-wallet');
        $I->seeResponseIsJson();
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseContainsJson(['data' => ['value' => $expected['wallet_value']]]);
    }
}
