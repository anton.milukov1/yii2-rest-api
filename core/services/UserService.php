<?php

namespace app\core\services;

use app\core\exceptions\InternalException;
use app\core\models\User;
use app\core\requests\JoinRequest;
use app\core\types\UserStatus;
use app\modules\v1\modules\user_wallet\models\Currency;
use app\modules\v1\modules\user_wallet\models\UserWallet;
use Exception;
use sizeg\jwt\Jwt;
use Yii;
use yii\db\ActiveRecord;
use yiier\helpers\Setup;

class UserService
{
    /**
     * @param JoinRequest $request
     * @return User
     * @throws InternalException
     */
    public function createUser(JoinRequest $request): User
    {
        $user = new User();
        try {
            // create user
            $user->username = $request->username;
            $user->email = $request->email;
            $user->setPassword($request->password);
            $user->generateAuthKey();
            if (!$user->save()) {
                throw new \yii\db\Exception(Setup::errorMessage($user->firstErrors));
            }

            // create wallet
            $wallet = new UserWallet();
            $wallet->user_id = $user->primaryKey;
            $wallet->currency_id = $request->walletCurrencyId;
            $wallet->value = 0;
            if (!$wallet->save()) {
                throw new \yii\db\Exception(Setup::errorMessage($wallet->firstErrors));
            }

        } catch (Exception $e) {
            Yii::error(
                ['request_id' => Yii::$app->requestId->id, $user->attributes, $user->errors, (string)$e],
                __FUNCTION__
            );
            throw new InternalException($e->getMessage());
        }
        return $user;
    }


    /**
     * @param User|null $user
     * @return string
     * @throws InternalException
     * @throws \Throwable
     */
    public function getToken(User $user = null): string
    {
        /** @var Jwt $jwt */
        $jwt = Yii::$app->jwt;
        if (!$jwt->key) {
            throw new InternalException(t('app', 'The JWT secret must be configured first.'));
        }
        $signer = $jwt->getSigner('HS256');
        $key = $jwt->getKey();
        $time = time();
        $username = $user ? $user->username : \user('username');
        $userId = $user ? $user->primaryKey : \user('id');
        return (string)$jwt->getBuilder()
            ->issuedBy(params('appUrl'))
            ->identifiedBy(Yii::$app->name, true)
            ->issuedAt($time)
            ->expiresAt($time + 3600 * 72)
            ->withClaim('username', $username)
            ->withClaim('id', $userId)
            ->getToken($signer, $key);
    }


    /**
     * @param string $value
     * @return User|ActiveRecord|null
     */
    public static function getUserByUsernameOrEmail(string $value)
    {
        $condition = strpos($value, '@') ? ['email' => $value] : ['username' => $value];
        return User::find()->where(['status' => UserStatus::ACTIVE])
            ->andWhere($condition)
            ->one();
    }

    /**
     * @param string $userName
     * @return bool
     * @throws \yii\db\StaleObjectException
     */
    public function deleteUserByUsername(string $userName): bool
    {
        $existed = User::findOne(['username' => $userName]);
        if (!$existed) return false;

        $userId = $existed->primaryKey;
        $wallet = UserWallet::findOne(['user_id' => $userId]);
        if ($wallet){
            Yii::$app->getDb()->createCommand("delete from user_wallet_transaction where wallet_id = {$wallet->primaryKey}");
            $wallet->delete();
        }
        $existed->delete();
        return true;
    }
}
